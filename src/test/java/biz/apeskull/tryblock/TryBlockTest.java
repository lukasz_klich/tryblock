package biz.apeskull.tryblock;

import com.google.common.base.Supplier;
import org.junit.Test;

import static biz.apeskull.tryblock.TryBlock.tryDo;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by lukasz on 07.04.14.
 */
public class TryBlockTest {

    private ThrowingAction<String> failing() {
        return new ThrowingAction<String>() {
            @Override
            public String apply() throws Throwable {
                throw new Exception();
            }
        };
    }

    private ThrowingAction<String> failing(final Throwable t) {
        return new ThrowingAction<String>() {
            @Override
            public String apply() throws Throwable {
                throw t;
            }
        };
    }

    private ThrowingAction<String> success(final String returnValue) {
        return new ThrowingAction<String>() {
            @Override
            public String apply() throws Throwable {
                return returnValue;
            }
        };
    }

    @Test
    public void tryDoShouldNotLetExceptionsOut() {
        try {
            tryDo(failing());
        } catch (Exception ex) {
            fail("Failed with throwned exception " + ex);
        }
    }

    @Test
    public void orReturnValueWhenOtherFailed() {
        //given
        String optionalResult = "optional result";

        //when
        String result = tryDo(failing())
                .orDo(failing())
                .or(optionalResult);

        //then
        assertThat(result).isEqualTo(optionalResult);
    }

    @Test
    public void orDoReturnFirstPresentValue() {
        //given
        String expected = "expected result";

        //when
        String result = tryDo(failing())
                .orDo(failing())
                .orDo(success(expected))
                .orDo(success("don't matter anymore"))
                .or("default value also doesn't matter");

        //then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void orThrowThrowsIfNothingReturnsValue() {
        //given
        String msg = "from orThrow";
        final Exception ex = new Exception(msg);

        try {
            //when
            tryDo(failing())
                    .orDo(failing())
                    .orThrow(new Supplier<Throwable>() {
                        @Override
                        public Throwable get() {
                            return ex;
                        }
                    });
            fail();
        } catch (Throwable t) {
            //then
            assertThat(t.getMessage()).isEqualTo(msg);
        }
    }

    @Test
    public void getReturnsEvaluatedValue() {
        //given
        String value = "this is it";

        //when
        String result = null;
        try {
            result = tryDo(failing())
                    .orDo(failing())
                    .orDo(success(value))
                    .orDo(failing())
                    .get();
        } catch (Throwable throwable) {
            fail();
        }

        //then
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void getThrowsLastException() {
        //given
        String msg = "msg";

        //when
        try {
            tryDo(failing(new Throwable()))
                    .orDo(failing(new Exception("msg")))
                    .get();
        } catch (Exception ex) {
            assertThat(ex.getMessage()).isEqualTo(msg);
        } catch (Throwable throwable) {
            fail();
        }
    }

    @Test
    public void getThrowsExceptionWithAllSuppressedExceptions() {
        try {
            //when
            tryDo(failing())
                    .orDo(failing())
                    .orDo(failing())
                    .get();
        } catch (Throwable t) {
            //then
            assertThat(t.getSuppressed().length).isEqualTo(2);
        }
    }

    @Test
    public void orThrowContainsSuppressedExceptions() {
        //given
        try {
            //when
            tryDo(failing())
                    .orDo(failing())
                    .orDo(failing())
                    .orThrow(new Supplier<Throwable>() {
                        @Override
                        public Throwable get() {
                            return new Throwable();
                        }
                    });
        } catch (Throwable t) {
            //then
            assertThat(t.getSuppressed().length).isEqualTo(3);
        }
    }
}

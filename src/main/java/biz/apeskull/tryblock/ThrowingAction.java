package biz.apeskull.tryblock;

import javax.annotation.Nullable;

/**
 * Created by lukasz on 07.04.14.
 */
public interface ThrowingAction<T> {
    @Nullable
    T apply() throws Throwable;
}

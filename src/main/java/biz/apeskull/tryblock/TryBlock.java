package biz.apeskull.tryblock;

import com.google.common.base.Optional;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Optional.fromNullable;

/**
 * Created by lukasz on 07.04.14.
 */
public class TryBlock<T> {

    private final Optional<T> result;
    private final ImmutableList<Throwable> suppressed;

    private TryBlock(Optional<T> result, ImmutableList<Throwable> suppressed) {
        this.result = result;
        this.suppressed = suppressed;
    }

    private static <T> TryBlock<T> tryDo(ThrowingAction<T> function, ImmutableList<Throwable> suppressed) {
        T result = null;
        ImmutableList<Throwable> newSuppressed = ImmutableList.copyOf(suppressed);
        try {
            result = function.apply();
        } catch (Throwable throwable) {
            newSuppressed = new ImmutableList.Builder<Throwable>()
                    .addAll(suppressed)
                    .add(throwable)
                    .build();
        }
        return new TryBlock(fromNullable(result), newSuppressed);
    }

    public static <T> TryBlock<T> tryDo(ThrowingAction<T> function) {
        return tryDo(function, ImmutableList.<Throwable>of());
    }

    public TryBlock<T> orDo(final ThrowingAction<T> fallback) {
        if (result.isPresent()) {
            return this;
        }
        return tryDo(fallback, suppressed);
    }

    public T or(final T defaultValue) {
        if (result.isPresent()) {
            return result.get();
        }
        return defaultValue;
    }

    public T orThrow(Supplier<Throwable> supplier) throws Throwable {
        if (result.isPresent()) {
            return result.get();
        }
        Throwable t = supplier.get();
        for (Throwable ignored : suppressed) {
            t.addSuppressed(ignored);
        }
        throw t;
    }

    public T get() throws Throwable {
        if (result.isPresent()) {
            return result.get();
        }
        for (Throwable ignored : suppressed.subList(0, suppressed.size() - 1)) {
            suppressed.reverse().get(0).addSuppressed(ignored);
        }
        throw suppressed.reverse().get(0);
    }
}
